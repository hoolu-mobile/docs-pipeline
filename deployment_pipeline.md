# 1. Introduction
## 1.1 Purpose
       The document describes the automated steps or pipeline android java source code goes through to build, configuration, tests and deployment before it reachs the google play store or any other store, from source code on a developers machine to the play store to be accessed for User acceptance testing and final users.

Part of the pipeline includes code styles checks to ensure all code is following agreed rules of formating. 

We describe the Versioning and version control work flow, build,running automated unit and other tests, configuration management and automated deployments to available environments. 

This document is not an introduction to these subjects, please access the references and usefull resources to get introduction and reference material.

## Why is this important.
We want to limit human intervation in the testing and deployment process to free developers to work on other more interesting things. 

## 1.2 Overview

## 1.4 Definitions and Acronyms

    Table 1. Definition, Acronyms and tools

| Term | Description |
| --- | --- |
| Testing Environment | Combination of the technology stack, services and hardware to run the application and run tests|
| Production Environment | Combination of the technology stack, services and hardware to run the application in production |
|version control | Git with gitlab.com|
| Docker Container |   |
| CI | Continous Integration  |
| CD | Continous Delivery  |
| Artifact Repository | A collection of binary software artifacts and metadata stored in a defined directory structure after or during a build process  |


# 2. CI Overview
    This section discusses the pipeline that enables us to achive CI, automated testing and deployments.

## 2.1 CI Architectural Model

### 2.1.1 Overview Diagram

* Presentation

![alt text](images/pipeline_architecture.jpg "Architecture")


####  Workflow,  How every thing comes together.

     1. After working on a piece of code on your development machine, this may be a new feature, bug fix or improvement.
     2. You commit your changes to your local working repository, push your changes and create a merge request on gitlab.com, our remote repo management system.

     2. The CI Server runs the commit stage that builds and runs unit tests.
     3. On successful completion, the binary as well as any reports and metadata are saved to the artifact repository.
     4. The CI server then retrieves the binaries created by the commit stage and deploys to a production-like test 
       environment.
     5. The CI server then runs the acceptance tests, reusing the binaries created by the commitstage.
     6. On successful completion, the release candidate is marked as having passed the acceptance tests.
     7. Testers can obtain a list of all builds that have passed the acceptance tests, and can press a button to run the 
       automated process to deploy them into a manual testing environment.
     8. Testers perform manual testing.
     9. On successful conclusion of manual testing, testers update the status of the release candidate to indicate it has 
       passed manual testing.
    10.The CI server retrieves the latest candidate that has passed user acceptance testing, or manual testing from the 
       artifact repository and deploys the application to the production test environment.
    11.Once the Release Candidate has passed through all of the relevant stages, it is ready for release,
       and can be released by anybody with the appropriate authorization, usually a combination of sign-off by QA and
       operations people.
    12.At the conclusion of the release process, the RC is marked as released


* Environment and Tools
    
      Table 2. Environment and Tools

| Term | Tool / Environment |
| --- | --- |
| Version Control | Gitlab.com for remote repository management and git on local machines |
| Testing Environment | Android OS variants, google play store account, Android mobile devices hardware and configuration |
| Production Environment | Android OS variants, google play store account, Android mobile devices hardware and configuration |
| CI Server | Gitlab CI |
| CI Agent | Gitlab Runner |
| Build Container | Docker Container |
| Build Tool | Gradle |



### 2.1.2 Continous Integration
    A development practice that requires developers to integrate code into a shared repository several times a day, each check in
    is verifid by an automated build allowing teams to detect problems early as described under the stages below.

Table 2. Continous Integration Stages

| Stage | Description | Tasks |
| --- | --- | --- |
| Commit Stage | its main purpose is to provide fast feedback for developers and prepare application binaries | build code,run unit tests,Create binaries for later stages,Perform code analysis (Code coverage, Cyclomatic complexity, Coupling, Code style) |
| Auto Acceptance Stage | Its main purpose is to ensure that you are testing the business acceptance criteria of your application, that is,validating that it provides users with valuable functionality and delivers value to customers. | run smoke and acceptance tests |

### Docker
We have wrapped our CI process with docker that guarantees that 
    
- each developer has the same build environment
- Docker containers are used to further quicken the process of the build without downloading dependencies every time a
  commit is made and also enables parallel execution of tasks
- Containers have no boot-up time so execution starts immediately,that makes containerization faster.

#### a) The Docker Integration workflow
* Presentation

![alt text](images/docker.jpg "Docker Workflow")


* Workflow

    1. Once a Developer has pushed a commit of the Dockerfile into Version Control system a build is triggered by CI Agent which launches the Docker engine/daemon.
    2. The CI Agent requests docker daemon to login and build the image
    3. Once the image is built, the CI Agent requests docker daemon to run the image and carry out tests on the container
    4. If the container passes the tests the CI Agent requests the docker daemon to push the image into the docker registry to be used by the build process
  
#### b) CI Workflow

* Presentation

 ![alt text](images/continous_integration.jpg "CI Workflow")

* workflow

 1. Once a Developer has pushed a change into Version Control system a build is triggered by CI Agent.
 2. The CI Agent grabs the commit and starts the docker engine
 3. The CI Agent pulls the image from the Docker Registry 
 4. The CI Agent runs the image and starts a Docker container with workspace mounted as a volume that includes the project files 

   -  If its the commit stage of the pipeline
       1. The CI Agent invokes gradle build tool inside the container to build the app
       2. Gradle then builds the app, runs unit tests and produces the apk which will be made available to the CI Agent from the container due to volume mounting
       3. Finally the CI Agent stores the resulting apk into the artifact repository, so that they can be used by other stages

   -  If its the acceptance stage
       1. The CI Agent retrieves the apk built by the commit stage 
       2. The CI Agent invokes gradle build tool inside the container
       3. Gradle runs a smoke test (to make sure the application is running and any services it depends on) and acceptance test suite

  5. The CI Agent pushes the resulting reports and meta data into the artifact repository


* Rationale

    - Manual testing usually happens at a time in the project where teams are under extreme pressure to get software out of the door. As a result, insufﬁcient time is normally planned to ﬁx the defects found as part of manual acceptance testing. Finally, when defects are found that require complex ﬁxes, there is a high chance of introducing further regression problems into the application.
    - Since the feedback loop is much shorter, defects are found much sooner, when they are cheaper to ﬁx. Second, since testers, developers, and customers need to work closely to create a good automated acceptance test suite, there is much better collaboration between them, and everybody is focused on the business value that the application is supposed to deliver.

    
### 2.1.3 Continous Deployment
  A development practice that requires developers to release into production of software that passes automated tests
  We have broken it down into two stages
    
| Stage | Description | Tasks |
| --- | --- | --- |
| User Acceptance Test Stage | This is where User Acceptance testing takes place, it involves human verification of the application | Exploratory testing,Usability testing,Showcases |
| Production Stage | Its aim is to deliver the application to end users thus sending packaged software to a stores or websites where they can be downloaded | deliver application to a production environment |


#### a) The CD Workflow

 It normally allows testers and other interested parties to deploy the application with one click on a chosen environment.

* Presentation

![alt text](images/continous_deployment.jpg "CD Workflow")

* Workflow

 1. After a developer merges code into the master branch the CI Agent activates the deploy button for that build
 2. The CI Agent pulls a run-time configuration from version control (e.g. gitlab.yaml + environment-specific configuration files)
 3. The CI Agent pulls the application from the artifact repository.

   -  If its the UAT stage
       1. The Developer/ Tester then performs a push button deploy
       2. The CI Agent then invokes gradle to run the deployment task
       3. The Gradle build tool then runs the task and deploys the apk to the Beta Testing Segment of the playstore

   -  If its the Production Stage
       1. The Developer then performs a push button deploy
       2. The CI Agent then invokes gradle to run the deployment task
       3. Gradle optimizes, signs and uploads the apk to the production segemnt of the playstore
 

## 2.2. CI/CD Behavioral Model

    Describes how changes propagate in the workflow

* Presentation

![alt text](images/pipeline_sequence.png "Sequence")

* Description

    1. The Delivery team makes a commit to version Control System. 
    This, in turn, triggers the ﬁrst stage in the pipeline (build and unit tests).
    If this fails a feedback message is sent to the development team. The team checks into the VCS again with changes when this passes,
    and triggers the second stage: the automated acceptance tests. When they fail a feedback is received by the team

# 3. Version Control Branching Workflow
## 3.1 Overview Diagram
     
* Presentation

![alt text](images/git_flow.png "git flow")

* Workflow 
     1. A master branch is created first
     2. A develop branch is created from master
     3. Feature branches are created from develop
     4. When a feature is complete it is merged into the develop branch
     5. When the develop branch has got enough features for a release, a release branch is created from develop
     6. When the release branch is done it is merged into develop and master
     7. If an issue in master is detected a hotfix branch is created from master
     8. Once the hotfix is complete it is merged to both develop and master
 
## 3.2 The Branches

### 3.2.1 The main branches

* The master branch

     - It stores the official release history
     - Deployment to production environment is done on this branch

* The develop branch

     - Serves as an integration branch for features , any time a release can be produced from this branch once the features are ready
     - Branches off of master

### 3.2.2 The Supporting branches

     These branches ease tracking of features, prepare for production releases, and fix production issues,
     these branches may have a limited time since they will be removed eventually

* The feature branch

    - Are used to develop new features for a release
    - Branching naming convention: username/feature
    - May branch off of develop when creating a new feature and must merge back into develop after finishing the feature
    - The feature branch will exist in the origin to allow collaboration and to enable the develop branch to be always ready for a new release without breaking it.
    - Unit testing and auto acceptance testing will be performed on this branch
    - The number of active branches that exist at any time must be limited to the number of stories in play. Nobody should start a new branch unless the branch representing their previous story is merged back to mainline
    - Only allow developers to merge to trunk once a story has been accepted
    - The tech lead has the right to reject patches that may potentially break the trunk.

* The Release branch

    - Release branches support preparation of a new release
    - Allow for minor bug fixes and preparing meta-data for release ( version number, build dates) by doing all this on the release branch the develop branch is cleared to receive features for the next big release
    - The key moment to branch off develop is when the develop almost reflects the desired state of a new release
    - The release gets assigned a version number
    - Deployment to testing environment is done on this branch
    - When the state of the release branch is ready it is merged into master and the commit is tagged for easy future reference.
    - The changes made on the release branch need to be merged back into develop so that future releases don&#39;t contain these bugs after the release branch may be removed because it won&#39;t be needed



* Hot fix branches

    - May branch off from master and merge back into develop and master in order to safeguard that the bug fix is included
    - And if the release branch still exists it should be merged into the release not develop branch because a release is eventually merged back into develop
    - Branch naming convention: hotfix-\*
    - Are used when there is an undesired state of a live production version when a critical bug must be resolved immediately
    - The hotfix branch is created so that work of team members must continue, while another person prepares the fix

## 1.3 References and usefull resources.

   [1] Atlassian, 'Gitflow Workflow'. [Online]. Available: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

   [2] David Farley and Jez Humble, "Continuous Delivery: Reliable Software Releases Through Build, Test, and Deployment Automation", Copyright © 2011 Pearson Education, Inc ,2010
